<?php


class Trollweb_Bring_Block_Tooltip extends Mage_Core_Block_Template
{

    protected $_infoArray;
  
    public function isTooltipActive()
    {
        return Mage::getStoreConfig('carriers/bring_fraktguiden/tooltip_active');
    }
  
    public function getTooltips() {
  
          $infoArray = $this->_getInfoArray();
  
          if (!is_array($infoArray)) {
            return array();
          }
          else {
            return $infoArray;
          }
    }
  
    protected function _getInfoArray()
    {
      if (!isset($this->_infoArray)) {
          $session = Mage::getSingleton('checkout/session');
          if ($session) {
            $this->_infoArray = $session->getTrollwebBringInfo();
          }
      }
  
      return $this->_infoArray;
    }

}
