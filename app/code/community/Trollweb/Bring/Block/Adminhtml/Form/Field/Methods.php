<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2010 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */

class Trollweb_Bring_Block_Adminhtml_Form_Field_Methods extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
   protected $_shippingmethodRenderer;
   protected $_allowfreeshippingRenderer;


    public function __construct()
    {
      $this->_prepareToRender();
      parent::__construct();
    }

   /**
     * Prepare to render
     */
    public function _prepareToRender()
    {
        $this->addColumn('shipping_method', array(
            'label' => Mage::helper('bring')->__('Fraktmetode'),
            'style' => 'width:120px',
            'renderer' => $this->_getShippingMethods(),
        ));
        $this->addColumn('custom_price', array(
            'label' => Mage::helper('bring')->__('Egendefinert pris'),
            'style' => 'width:60px',
        ));
        $this->addColumn('price_to_no', array(
            'label' => Mage::helper('bring')->__('Pris til NO'),
            'style' => 'width:60px',
        ));
        $this->addColumn('price_to_se', array(
            'label' => Mage::helper('bring')->__('Pris til SE'),
            'style' => 'width:60px',
        ));
        $this->addColumn('price_to_dk', array(
            'label' => Mage::helper('bring')->__('Pris til DK'),
            'style' => 'width:60px',
        ));
        $this->addColumn('price_to_fi', array(
            'label' => Mage::helper('bring')->__('Pris til FI'),
            'style' => 'width:60px',
        ));
        $this->addColumn('min_weight', array(
            'label' => Mage::helper('bring')->__('Min weight'),
            'style' => 'width:60px',
        ));
        $this->addColumn('max_weight', array(
            'label' => Mage::helper('bring')->__('Max weight'),
            'style' => 'width:60px',
        ));        
        $this->addColumn('allow_free_shipping', array(
            'label' => Mage::helper('bring')->__('Tillat gratis frakt'),
            'style' => 'width:50px',
            'id'    => 'allow_free_shipping',
            'renderer' => $this->_getAllowFreeShipping(),
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add method');
    }

    /**
     * Retrieve group column renderer
     *
     * @return Mage_CatalogInventory_Block_Adminhtml_Form_Field_Customergroup
     */
    protected function _getShippingMethods()
    {
        if (!$this->_storedescRenderer) {
          if ($this->getLayout()) {
            $this->_storedescRenderer = $this->getLayout()->createBlock(
                'bring/adminhtml_form_field_methodselect', '',
                array('is_render_to_js_template' => true)
            );
          }
        }
        return $this->_storedescRenderer;
    }

    protected function _getAllowFreeShipping()
    {
      if (!$this->_allowfreeshippingRenderer) {
        if ($this->getLayout()) {
          $this->_allowfreeshippingRenderer = $this->getLayout()->createBlock(
          			'bring/adminhtml_form_field_checkbox', '',
                array('is_render_to_js_template' => true, 'id' => 'allow_free_shipping')
          );
        }
      }
      return $this->_allowfreeshippingRenderer;
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {

        $row->setData(
            'option_extra_attr_' . $this->_getShippingMethods()->calcOptionHash($row->getData('shipping_method')),
            'selected="selected"'
        );

        $row->setData('allow_free_shipping_checked', ($row->getData('allow_free_shipping') ? 'checked' : '') );

    }

}
