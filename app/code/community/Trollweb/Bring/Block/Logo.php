<?php

class Trollweb_Bring_Block_Logo extends Mage_Core_Block_Template
{


    public function getLogo()
    {
        $logoUrl = "";
        $country = Mage::getStoreConfig("shipping/origin/country_id");

        if ($country === "NO") {
          $logoUrl = $this->getSkinUrl('bring/logo-posten.png');
        } else {
          $logoUrl = $this->getSkinUrl('bring/logo-bring.gif');
        }

        return $logoUrl;
    }


    public function getCarrierName()
    {
        $carrierCode = $this->getCode();
        if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title')) {
            return $name;
        }   
        return $carrierCode;
    }
}
