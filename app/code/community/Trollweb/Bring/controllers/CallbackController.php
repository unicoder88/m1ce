<?php


class dummyClass
{
	function getStoreId()
	{
		return Mage::app()->getStore()->getId();
	}
}


class Trollweb_Bring_CallbackController extends Mage_Checkout_Controller_Action
{
    public function indexAction()
    {
		$request = $this->getRequest();
		
		$data = array(
			'productCode' => $this->getRequest()->getParam('productCode'),
			'machineId' => $this->getRequest()->getParam('machineId'),
			'machineName' => $this->getRequest()->getParam('machineName'),
			'address' => $this->getRequest()->getParam('address'),
			'postalCode' => $this->getRequest()->getParam('postalCode'),
			'city' => $this->getRequest()->getParam('city'),
			'locationDescription' => $this->getRequest()->getParam('locationDescription'),
			'bookingNumber' => $this->getRequest()->getParam('bookingNumber'),
			'shippingDate'=> $this->getRequest()->getParam('shippingDate'),
			'type' => $this->getRequest()->getParam('type'),
			'correlationId' => $this->getRequest()->getParam('correlationId'),
			'publicId' => $this->getRequest()->getParam('publicId')
		);
        
        /* Save in session */
        $session = Mage::getSingleton('checkout/session');
        $session->setData('postautomat', $data);
		
        /* Gather price using API */
        $bringRequest = Mage::getModel('bring/Shipping_Fraktguiden_Request');
        $shipping_address = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress();
 
        // Initial setup
        $bringRequest->setUsercode(Mage::getStoreConfig('carriers/bring_fraktguiden/usercode'));
        $bringRequest->setFromPostalCode(Mage::getStoreConfig('shipping/origin/postcode', Mage::app()->getStore()));
        $bringRequest->setFromCountryCode(Mage::getStoreConfig('shipping/origin/country_id', Mage::app()->getStore()));        
        $bringRequest->setToPostalCode($data['postalCode']);
        $bringRequest->setToCountryCode($shipping_address->getCountryId());
		
		
		
		
		// Calculate weight
		$weight = 0;
	
		// Calculate package weight by looping trought all the items and getting the correct weight.
		if ((Mage::getStoreConfig('carriers/bring_fraktguiden/weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT) or
			(Mage::getStoreConfig('carriers/bring_fraktguiden/weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT_KG)) 
		{
			foreach (Mage::getSingleton('checkout/session')->getQuote()->getAllItems() as $item) {
			if (($item->getWeight() == 0) and Mage::getStoreConfig('carriers/bring_fraktguiden/use_average_on_nullweight')) {
				$weight += $item->getQty()*(int)Mage::getStoreConfig('carriers/bring_fraktguiden/product_weight');
			}
			else {
				$weight += $item->getQty()*$item->getWeight();
			}
			}
	
			// if weight is in KG - multiply with 1000 to get it in Grams.
			if (Mage::getStoreConfig('carriers/bring_fraktguiden/weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT_KG) {
			$weight = $weight*1000;
			}
		}
		else {
		$weight = ceil($request->getPackageQty()*(int)Mage::getStoreConfig('carriers/bring_fraktguiden/product_weight'));
		}	
        
		$bringRequest->setPackageGrossWeight($weight);
        
        $bringClient = new Trollweb_Bring_Model_Shipping_Fraktguiden_SoapClient(Trollweb_Bring_Model_Shipping_Fraktguiden::FRAKTGUIDEN_WSDL,
            array('trace'=>1));     
        
   
        $productCode = $data['productCode'];
		

		
		if($productCode == '3020')	{
			$productCode = 'POSTAUTOMAT_NEXTDAY';
		}
		elseif($productCode == '3019')	{
			$productCode = 'POSTAUTOMAT_SAMEDAY';
		}
		elseif($productCode == '3021')	{
			$productCode = 'MYQUICKBOX_SAMEDAY';
		}
		elseif($productCode == '3022')	{
			$productCode = 'MYQUICKBOX_NEXTDAY';
		}
		
        
        $bringRequest->setPackageLength( Mage::getStoreConfig('carriers/bring_fraktguiden/default_length') );
        $bringRequest->setPackageHeight( Mage::getStoreConfig('carriers/bring_fraktguiden/default_height') );
        $bringRequest->setPackageWidth( Mage::getStoreConfig('carriers/bring_fraktguiden/default_width') );
        
        
        $bringRequest->setProductId($productCode);
        $bringResult = $bringClient->getPrice($bringRequest);   
      
	  	$dummy = new Mage_Shipping_Model_Rate_Request();
	  
	  	if (!property_exists($bringResult->Packages->Package,"Product")) {
		
		}
		else	{
			$price = $bringResult->Packages->Package->Product->Price->PackagePriceWithoutAdditionalServices->AmountWithVAT;
			$session->setData('postautomat_price', $price);
		}
		
		Mage::getSingleton('checkout/type_onepage')->saveShippingMethod('bring_fraktguiden_POSTAUTOMAT_NEXTDAY');
	  
		$response = <<<EOF
<script>

	/* Open previous section */
	window.opener.accordion.openPrevSection();
	
	var current = window.opener.accordion.currentSection;
	
	if(current == 'opc-shipping')	{
		window.opener.shipping.save();
	}
	else if(current == 'opc-billing')	{
		window.opener.billing.save();
	}

	/*window.opener.updatePostautomatValg('{$data['productCode']}','{$data['machineId']}','{$data['machineName']}','{$data['address']}', '{$data['postalCode']}', '{$data['city']}', '{$data['locationDescription']}', '{$data['bookingNumber']}', '{$data['shippingDate']}', '{$data['type']}', '{$data['correlationId']}', '{$data['publicId']}');*/


window.close();
</script>
EOF;

		
		$this->getResponse()->setBody($response);

		return;
    }

}


?>