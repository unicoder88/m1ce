<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'bring_disable', array(
        'type'              => 'int',
        'label'             => 'Deaktiver Bring',
        'input'             => 'boolean',
        'required'          => false,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));


$installer->addAttribute('catalog_product', 'bring_nocod', array(
        'type'              => 'int',
        'label'             => 'Deaktiver Bring oppkrav',
        'input'             => 'boolean',
        'required'          => false,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
/*
$installer->addAttribute('catalog_product', 'bring_width', array(
        'type'              => 'int',
        'label'             => 'Bring width',
        'input'             => 'text',
        'required'          => false,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

$installer->addAttribute('catalog_product', 'bring_height', array(
        'type'              => 'int',
        'label'             => 'Bring height',
        'input'             => 'text',
        'required'          => false,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

$installer->addAttribute('catalog_product', 'bring_length', array(
        'type'              => 'int',
        'label'             => 'Bring length',
        'input'             => 'text',
        'required'          => false,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

*/
/*
$installer->addAttribute('order', 'postautomat_product_code', array());
$installer->addAttribute('order', 'postautomat_machine_id', array());
$installer->addAttribute('order', 'postautomat_machine_name', array());
$installer->addAttribute('order', 'postautomat_address', array());
$installer->addAttribute('order', 'postautomat_postal_code', array());
$installer->addAttribute('order', 'postautomat_city', array());
$installer->addAttribute('order', 'postautomat_location_description', array());
$installer->addAttribute('order', 'postautomat_booking_number', array());
$installer->addAttribute('order', 'postautomat_shipping_date', array());
$installer->addAttribute('order', 'postautomat_type', array());
$installer->addAttribute('order', 'postautomat_correlation_id', array());
$installer->addAttribute('order', 'postautomat_public_id', array());
*/


$installer->endSetup();
