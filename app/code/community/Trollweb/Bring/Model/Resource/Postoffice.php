<?php

class Trollweb_Bring_Model_Resource_Postoffice extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct()
    {
        $this->_init('bring/postoffices', 'id');
    }
}
