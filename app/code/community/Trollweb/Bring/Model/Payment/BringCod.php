<?php

/**
 * Bring Cash on Delivery (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */

class Trollweb_Bring_Model_Payment_BringCod extends Mage_Payment_Model_Method_Abstract
{

    /**
    * unique internal payment method identifier
    * @var string [a-z0-9_]
    */
    protected $_code = 'bringcod';
    protected $_customCod;



    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if ($this->getCheckout()->getQuoteId()) {
          return $this->getCheckout()->getQuote();
        }
        else {
          return false;
        }
    }

    /**
     * Return true if the method can be used at this time
     *
     * @return bool
     */
    public function isAvailable($quote=null)
    {
    	$isAvailable = false;

    	if ($this->getConfigData('active') and $quote) {
	      $shipping_method = $quote->getShippingAddress()->getShippingMethod();
	      $codTypes = $this->getCodTypes();

	      $methods = explode("_",$shipping_method,3);
	      if (is_array($methods)) {
            $carrier = array_shift($methods);
	        $shipping_method = array_shift($methods);
          }

	      if (array_key_exists($shipping_method,$codTypes)) {
          $isAvailable = true;
        	if ($this->getConfigData('hideone_attribute')) {
        		// Check the cart for "bring_nocod" items.
		        $all_items = $quote->getAllItems();
		        foreach ($all_items as $item) {
		        	$product = Mage::getModel('catalog/product')
                            ->setStoreId($quote->getStoreId())
                            ->load($item->getProductId());
              foreach ($product->getAttributes() as $attribute => $obj) {
			          if (($attribute == "bring_nocod") and ($product->getData($attribute) == 1)) {
			            $isAvailable = false;
			          }
              }
		          unset($product);
		        }
        	}

        	$total = $quote->getTotals();
        	$max_amount = $this->getConfigData('hideon_amount');
        	if ($max_amount && ($total['subtotal']->getValueInclTax() > $max_amount))
        	{
        	  $isAvailable = false;
        	}
	      }
    	}

      return $isAvailable;
    }

    public function getTitle()
    {
      $quote = $this->getQuote();
      $fee = false;
      $shipping_method = false;
      $text = '';

      if ($quote) {
        $shipping_method = $quote->getShippingAddress()->getShippingMethod();
      }

      if ($shipping_method) {
        if (Mage::helper('tax')->displayShippingPriceIncludingTax()) {
          $fee = $this->getCodFee($shipping_method,true);
        }
        else {
          $fee = $this->getCodFee($shipping_method,false);
        }
        $currencyCode = $quote->getBaseCurrencyCode();

        if ($fee) {
          $text = ' ('.$currencyCode.' '.number_format($fee,2,',',' ').')';
        }
        else {
          $text = ' '.Mage::helper('bring')->__('(no fee)');
        }
      }

      return $this->getConfigData('title').$text;
    }

    public function getCodFee($shipping_method,$IncVAT=false) {
      $codTypes = $this->getCodTypes();

      $methods = explode("_",$shipping_method,3);
      if (is_array($methods)) {
        $carrier = array_shift($methods);
        $shipping_method = array_shift($methods);
      }

      $fee = 0;
      if (!Mage::getSingleton('checkout/session')->getTrollWebBringFreeShipping() or $this->getConfigData('codfee_on_freeshipping')) {
        if (array_key_exists($shipping_method,$codTypes)) {
          if ($this->getCustomCodFee()) {
             $fee = $this->getCustomCodFee();
          }
          else {
            if ($IncVAT) { //if (Mage::helper('tax')->shippingPriceIncludesTax() || $forceIncVAT) {
              $fee = $codTypes[$shipping_method]->AmountWithVAT;
            }
            else {
              $fee = $codTypes[$shipping_method]->AmountWithoutVAT;
            }
          }
        }
      }

      return $fee;

    }

    public function getCustomCodFee()
    {
        if (!isset($this->_customCod)) {
            $_cod = $this->getConfigData('custom_codfee');
            if (!empty($_cod)) {
                $this->_customCod = $_cod;
            }
            else {
                $this->_customCod = false;
            }
        }

        return $this->_customCod;
    }

    private function getCodTypes() {
      $pArray = $this->getCheckout()->getTrollWebBringPostOppkrav();
      if (!is_array($pArray)) {
        $pArray = array();
      }
      return $pArray;
    }
}
