<?php
/**
 * Bring Cash on Delivery (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2011 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */

class Trollweb_Bring_Model_Payment_Quote_Total extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    public function __construct()
    {
        $this->setCode('shipping');
    }


    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        $address->setBaseBringCod(0);
        $address->setBringCod(0);

        $paymentMethod = Mage::app()->getFrontController()->getRequest()->getParam('payment');
        $paymentMethod = Mage::app()->getStore()->isAdmin() && isset($paymentMethod['method']) ? $paymentMethod['method'] : null;
        if ($paymentMethod != 'bringcod' && (!count($address->getQuote()->getPaymentsCollection()) || !$address->getQuote()->getPayment()->hasMethodInstance())){
            return $this;
        }

        $paymentMethod = $address->getQuote()->getPayment()->getMethodInstance();

        if ($paymentMethod->getCode() != 'bringcod') {
            return $this;
        }

        $items = $address->getAllItems();
        if (!count($items)) {
            return $this;
        }

        $baseTotal = $address->getBaseGrandTotal();
        $baseCodFee = 0;
        $baseCodNoTax = 0;

        $codamount = $paymentMethod->getCodFee($address->getShippingMethod());

        if (Mage::helper('tax')->shippingPriceIncludesTax()) {
          // Include tax
          $baseCodFee = $paymentMethod->getCodFee($address->getShippingMethod(),true);
        }
        else {
          // Exclude tax
          $baseCodFee = $paymentMethod->getCodFee($address->getShippingMethod(),false);
        }

        $baseCodNoTax = $paymentMethod->getCodFee($address->getShippingMethod(),false);

        if (!$baseCodFee > 0 ) {
            return $this;
        }

        // adress is the reference for grand total
        $quote = $address->getQuote();

        $store = $quote->getStore();

        $baseTotal += $baseCodFee;

        $address->setBaseBringCod($baseCodNoTax);
        $address->setBringCod($store->convertPrice($baseCodNoTax,false));

        $address->setBaseShippingAmount($address->getBaseShippingAmount()+$baseCodFee);
        $address->setShippingAmount($address->getShippingAmount()+$store->convertPrice($baseCodFee,false));

        $address->setShippingDescription($address->getShippingDescription().' + '.$paymentMethod->getTitle());


        return $address;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address) {

    }
 }
