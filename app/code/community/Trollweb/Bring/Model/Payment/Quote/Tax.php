<?php
/**
 * Bring Cash on Delivery (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2011 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */


class Trollweb_Bring_Model_Payment_Quote_Tax extends Mage_Sales_Model_Quote_Address_Total_Tax {

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {

        $address->setBringCodTaxAmount(0);
        $address->setBaseBringCodTaxAmount(0);

        $paymentMethod = Mage::app()->getFrontController()->getRequest()->getParam('payment');
        $paymentMethod = Mage::app()->getStore()->isAdmin() && isset($paymentMethod['method']) ? $paymentMethod['method'] : null;
        if ($paymentMethod != 'bringcod' && (!count($address->getQuote()->getPaymentsCollection()) || !$address->getQuote()->getPayment()->hasMethodInstance())){
            return $this;
        }

        $paymentMethod = $address->getQuote()->getPayment()->getMethodInstance();

        if ($paymentMethod->getCode() != 'bringcod') {
            return $this;
        }

        $items = $address->getAllItems();
        if (!count($items)) {
            return $this;
        }

        $store = $address->getQuote()->getStore();
        $custTaxClassId = $address->getQuote()->getCustomerTaxClassId();

        $taxCalculationModel = Mage::getSingleton('tax/calculation');
        /* @var $taxCalculationModel Mage_Tax_Model_Calculation */
        $request = $taxCalculationModel->getRateRequest($address, $address->getQuote()->getBillingAddress(), $custTaxClassId, $store);
        $shippingTaxClass = (int)Mage::getStoreConfig('tax/classes/shipping_tax_class', $store);
        $shippingPriceIncTax = (int)Mage::getStoreConfig('tax/calculation/shipping_includes_tax', $store);

        $codTax      = 0;
        $codBaseTax  = 0;

        if ($shippingTaxClass) {
            if ($rate = $taxCalculationModel->getRate($request->setProductClassId($shippingTaxClass))) {
              $sfTax    = $address->getBringCod() * $rate/100;
              $sfBaseTax= $address->getBaseBringCod() * $rate/100;

              $sfTax    = $store->roundPrice($sfTax);
              $sfBaseTax= $store->roundPrice($sfBaseTax);

              $address->setTaxAmount($address->getTaxAmount() + $sfTax);
              $address->setBaseTaxAmount($address->getBaseTaxAmount() + $sfBaseTax);

              $this->_saveAppliedTaxes(
                    $address,
                    $taxCalculationModel->getAppliedRates($request),
                    $sfTax,
                    $sfBaseTax,
                    $rate
              );
            }
        }

        $address->setShippingTaxAmount($address->getShippingTaxAmount()+$sfTax);
        $address->setBaseShippingTaxAmount($address->getBaseShippingTaxAmount()+$sfBaseTax);
      //  $address->setGrandTotal($address->getGrandTotal() + $address->getSimplefreightTaxAmount());
      //  $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseSimplefreightTaxAmount());
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        return $this;
    }
}
