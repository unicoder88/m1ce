<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */

class Trollweb_Bring_Model_Shipping_Config_Weighthandling
{
    public function toOptionArray()
    {
      return array(
        array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT, 'label' => Mage::helper('adminhtml')->__('Gram')),
        array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT_KG, 'label' => Mage::helper('adminhtml')->__('Kilogram')),
       // array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_CALCULATE, 'label' => Mage::helper('adminhtml')->__('Calculate (Items qty*Average product weight)')),
      );
    }

}
