<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 * 
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any 
 * part of it. Please contact us by email at post@trollweb.no or 
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 * 
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 * 
 */ 

class Trollweb_Bring_Model_Shipping_Config_Postingsite
{
    public function toOptionArray()
    {
      return array(
        array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::DELIVERY_TERMINAL, 'label' => Mage::helper('bring')->__('Postterminal')),
      	array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::DELIVERY_OFFICE, 'label' => Mage::helper('bring')->__('Postkontor')),
      );
    }
    
}