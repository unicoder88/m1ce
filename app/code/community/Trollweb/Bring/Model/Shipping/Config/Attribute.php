<?php

/**
 * Bring Fraktguiden Freight Integration (Norway)
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 * Visit us at http://www.trollweb.no today!
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2011 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 */
class Trollweb_Bring_Model_Shipping_Config_Attribute
{
    public function toOptionArray()
    {
        $arr = array();

        $entity =
            Mage::getModel('eav/entity_type')->loadByCode('catalog_product');
        $entity_type_id = $entity->getEntityTypeId();
        if (empty($entity_type_id)) {
            return $arr;
        }

        $attributesInfo = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setEntityTypeFilter($entity_type_id)
            ->getData();

        foreach ($attributesInfo as $attribute) {
            if (empty($attribute['frontend_label'])) {
                $attribute['frontend_label'] = $attribute['attribute_code'];
            }
            $arr[] = array(
                'value' => $attribute['attribute_code'],
                'label' => $attribute['frontend_label'],
                );
        }

        return $arr;
    }
}