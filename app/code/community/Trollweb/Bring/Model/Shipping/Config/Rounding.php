<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2012 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */


class Trollweb_Bring_Model_Shipping_Config_Rounding
{
    public function toOptionArray()
    {
        $methods = array(
          array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_NONE, 'label' => Mage::helper('bring')->__('Ingen avrunding')),
        	array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_NEAREST_INT, 'label' => Mage::helper('bring')->__('Rund til nærmeste krone')),
        	array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_NEAREST_TEN, 'label' => Mage::helper('bring')->__('Rund til nærmeste ti krone')),
        	array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_UP_INT, 'label' => Mage::helper('bring')->__('Rund opp til nærmeste krone')),
        	array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_UP_TEN, 'label' => Mage::helper('bring')->__('Rund opp til nærmeste ti krone')),
          array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_DOWN_INT, 'label' => Mage::helper('bring')->__('Rund ned til nærmeste krone')),
        	array('value' => Trollweb_Bring_Model_Shipping_Fraktguiden::ROUND_DOWN_TEN, 'label' => Mage::helper('bring')->__('Rund ned til nærmeste ti krone')),
        	              );
        return $methods;
    }
}