<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 * 
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 */
class Trollweb_Bring_Model_Shipping_Config_Backend_Methods
    extends Mage_Adminhtml_Model_System_Config_Backend_Serialized_Array
{
    /**
     * Prepare data before save
     */
    protected function _beforeSave()
    {
        $value = $this->getValue();
        if (is_array($value)) {
            unset($value['__empty']);

            $methods = array();
            foreach ($value as $key => $val) {
                $methods[] = $val['shipping_method'];
                $value[$key]['custom_price'] =
                    (float)$value[$key]['custom_price'];
                $value[$key]['allow_free_shipping'] =
                    (isset($value[$key]['allow_free_shipping']) && ($value[$key]['allow_free_shipping'] == 1) ?  '1' : '0');
            }

            $this->setValue($value);
            parent::_beforeSave();
        }
    }

    protected function _afterLoad()
    {
        if (!is_array($this->getValue())) {
            $unser = @unserialize($this->getValue());
            if ($unser) {
                $this->setValue(unserialize($this->getValue()));
            } else {
                // Try to load the old way.
                $methods = explode(",", $this->getValue());

                $info = array();
                foreach ($methods as $method) {
                    $info[] = array(
                        'shipping_method' => $method,
                        'custom_price' => 0,
                        'allow_free_shipping' => 1,
                        );
                }
                $this->setValue($info);
            }
        }
    }
}
