<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */

class Trollweb_Bring_Model_Shipping_Fraktguiden_Request extends Varien_Object
{



  public function SoapRequest() {
    $language = $this->getLanguage();
    // API wants code DA for denmark
    $language = str_replace("DK", "DA", $language);

  	$userInformation = array(
        "ClientURL"                          => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
    );

    $requestProperties = array(
        "SchemaVersion"                     => "11",
        "Language"                          => $this->_useDefault($language,'no'),
        "WithPrice"                         => $this->_useDefault($this->getWithPrice(),true),
        "WithExpectedDelivery"              => $this->_useDefault($this->getWidthExpectedDelivery(),true),
        "NumberOfAlternativeDeliveryDates"  => $this->_useDefault($this->getNumberOfAlternativeDeliveryDates(),'0'),
        "EDI"                               => $this->getEDI(),
        "PostingAtPostoffice"               => $this->_useDefault($this->getPostingAtPostoffice(),false),
        "Trace"                             => false,
        "WithPackageDetails"                => true,
        );

    $productIds = array();

    $mybringCreds = Mage::helper("bring")->getMybringCredentials();

    if ($mybringCreds) {
        $productIds = array('ProductId' => array("_" => $this->getProductId(), "customerNumber" => $mybringCreds["customerNumber"]));
    } else {
        $productIds = array('ProductId' => $this->getProductId());
    }

    $consignment = array(
        'FromCountryCode'                   => $this->getFromCountryCode(),
        'FromPostalCode'                    => str_pad($this->getFromPostalCode(),4,'0',STR_PAD_LEFT),
        'ToCountryCode'                     => $this->getToCountryCode(),                               
        'ToPostalCode'                      => str_pad($this->getToPostalCode(),4,'0',STR_PAD_LEFT),
        'ToCity'                            => '',
        'ShippingDate'                      => array(
            'Year'      => date("Y"),
            'Month'     => date("m"),
            'Day'       => date("d"),
            'Hour'      => '16',
            'Minute'    => '0',
            ),
        'EarlyCollectionAtTerminal' => '',
        'AdditionalServices' => array(
            "AdditionalService" => array(array( 
                "AdditionalServiceId"           => "POSTOPPKRAV",
                "AdditionalServiceParameters"   => array(),
                )),
            ),
        );
    $packages = array('Package' => array());

    // Width, Height and Length is defined.
    if ($this->getPackageHeight() and $this->getPackageWidth() and $this->getPackageLength()) {
      $packages['Package']['Height'] = $this->getPackageHeight();
      $packages['Package']['Width'] = $this->getPackageWidth();
      $packages['Package']['Length'] = $this->getPackageLength();
    }

    // Weight is defined.
    if ($this->getPackageGrossWeight()) {
      $packages['Package']['GrossWeight'] = $this->getPackageGrossWeight();
    }

    // Volume is defined.
    if ($this->getPackageVolume()) {
      $packages['Package']['Volume'] = $this->getPackageVolume();
      $packages['Package']['VolumeSpecial'] = $this->_useDefault($this->getPackageVolumeSpecial(),"false");
    }

    if ($this->getAdditionalServices()) {
        foreach ($this->getAdditionalServices() as $serviceId)
        {
            $consignment['AdditionalServices']['AdditionalService'][] = array(
                'AdditionalServiceId'           => $serviceId,
                'AdditionalServiceParameters'   => array()
                );
        }

    }

    $request_array = array(
    					'UserInformation' => $userInformation,
                        'RequestProperties' => $requestProperties,
                        'ProductIds' => $productIds,
                        'Consignment' => $consignment,
                        'Packages' => $packages
                          );

    return $request_array;
  }

  public function addAdditionalService($service) {
    $services = $this->getData('additional_services');
    if (!$services) {
      $this->setData('additional_services', array($service));
    } else {
      $services[] = $service;
      $this->setData('additional_services', $services);
    }

    return $this;
  }

  public function setAdditionalServices($service)
  {
    if (is_array($service)) {
        $this->setData('additional_services',$service);
    }
    else {
        $this->setData('additional_services',array($service));   
    }
    return $this;
  }

  private function _useDefault($var, $default) {
      return ($var ? $var : $default);
  }

}
