<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *

/**
* Bring Fraktguiden Soap Client
*/
class Trollweb_Bring_Model_Shipping_Fraktguiden_SoapClient extends SoapClient
{

    public function getPrice(Trollweb_Bring_Model_Shipping_Fraktguiden_Request $request) {
        $result = false;

        try {
            $debug = Mage::getStoreConfig('carriers/bring_fraktguiden/debug') === "1";
            if ($debug) {
                Mage::helper("bring")->log("Request:");
                Mage::helper("bring")->log($request->SoapRequest());
            }

            $result = $this->ShippingGuide($request->SoapRequest());

            if ($debug) {
                Mage::helper("bring")->log("Result:");
                Mage::helper("bring")->log($result);
            }
        }
        catch (SoapFault $e) {
            $this->dolog("**getPrice** (".$request->getProductId().") ".$e->faultstring,true);
            $result = false;
        }

        return $result;
    }

  private function dolog($logline)
  {
  	$logDir = Mage::getBaseDir('log');
  	$fh = fopen($logDir."/trollweb_bring.log","a");
  	if ($fh) {
  		fwrite($fh,"[".date("d.m.Y h:i:s")."] ".$logline."\n");
  		fclose($fh);
  	}
  }

}
