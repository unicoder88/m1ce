<?php

class Trollweb_Bring_Model_Shipping_Hent extends Trollweb_Bring_Model_Shipping_Fraktguiden
{
    protected $_code = 'bringhent';
    private $_allowedToCountries = array("NO", "DK", "SE", "FI");

    const PICKUPPOINT_API = 'https://api.bring.com/pickuppoint/api/pickuppoint';

    protected function appendResult($method)
    {
      if (!isset($this->_result)) {
          $this->_result = Mage::getModel('shipping/rate_result');
      }

      $toCountry = $this->getBringRequest()->getToCountryCode();

      if (!$this->getConfigData('choose_postoffice') || !in_array($toCountry, $this->_allowedToCountries)) {
          return parent::appendResult($method);
      }

      $max_postoffice = (int)$this->getConfigData('max_postoffice');
      if ($max_postoffice > 10)
      {
          $max_postoffice = 10;

      }
      if ($max_postoffice < 1 || $this->_missingDstPostcode)
      {
          $max_postoffice = 1;
      }
      $places = $this->getDeliveryPlaces();
      $places = array_slice($places, 0, $max_postoffice);

      // Reverse list of postoffices for php versions lower than 7
      // to get correct order in checkout
      if (PHP_MAJOR_VERSION < 7) {
          $places = array_reverse($places);
      }

      $maxDistanceInKm = (int)$this->getConfigData('max_postoffice_distance');

      $i = 0;
      foreach ($places as $place) {
          if ($maxDistanceInKm > 0 && $place["distanceInKm"] > $maxDistanceInKm) {
              continue;
          }

          $newPlace = clone $method;
          $newPlace->setMethod($method->getMethod().'_'.$place['id']);
          $newPlace->setMethodTitle($place['name']);
          $newPlace->setMethodDescription($this->formatMethodDescription($place, $toCountry));

          // Set generic title and remove description if we dont have a destination postcode
          if ($this->_missingDstPostcode) {
              $newPlace->setMethodTitle(Mage::helper("bring")->__("På posten"));
              $newPlace->setMethodDescription("");
          }

          $this->_result->append($newPlace);
      }

      return $this->_result;
    }

    private function formatMethodDescription($place, $toCountry) {
        if ($toCountry === "SE") {
            return $this->formatMethodDescriptionSE($place);
        } elseif ($toCountry === "FI") {
            return $this->formatMethodDescriptionFI($place);
        } else {
            return $this->formatMethodDescriptionNO($place);
        }
    }

    private function formatMethodDescriptionSE($place) {
        return sprintf('%s, %s %s', $place['address'], $place['postalCode'], $place['city']);
    }

    private function formatMethodDescriptionNO($place) {
        $description = "";

        // Add visiting address if present
        if (array_key_exists("visitingAddress", $place)) {
            $description .= $place['visitingAddress'] . ", ";
        }

        $description .= sprintf('%s %s<br>%s<br><a target="_blank" href="%s" class="mapLink">%s</a>',
            $place['visitingPostalCode'],
            $place['visitingCity'],
            $place['openingHoursNorwegian'],
            $place['googleMapsLink'],
            Mage::helper('bring')->__('Show map in new window')
        );

        return $description;
    }

    private function formatMethodDescriptionFI($place) {
        return sprintf('%s, %s %s<br>%s<br><a target="_blank" href="%s" class="mapLink">%s</a>',
            $place['address'],
            $place['postalCode'],
            $place['city'],
            $place['openingHoursEnglish'],
            $place['googleMapsLink'],
            Mage::helper('bring')->__('Show map in new window')
        );
    }

    protected function getDeliveryPlaces()
    {
        $request = $this->getBringRequest();
        $url = self::PICKUPPOINT_API."/". $request->getToCountryCode() . "/postalCode/" .$request->getToPostalCode().'.json?numberOfResponses=20';

        // Add street parameter to request if there is one
        $street = $request->getToStreet();
        if ($street) {
            $url .= "&" . http_build_query(array("street" => $street));
        }

        $json = @file_get_contents($url);

        if ($json) {
            $data = @json_decode($json,true);
            if (is_array($data) && isset($data['pickupPoint'])) {
                $this->savePostoffice($data['pickupPoint']);
                return $data['pickupPoint'];
            }
        }

        return false;
    }

    protected function savePostoffice($postoffices)
    {
        
        foreach ($postoffices as $office)
        {
            $id = $office['id'];
            $jsonData = json_encode($office);
            if (!$jsonData) {
                continue;
            }
            
            $po = Mage::getModel('bring/postoffice')->load($id,'postoffice_id');
            $po->setPostofficeId($id);
            if ($po->getPostofficeData() != $jsonData) {
                $po->setPostofficeData($jsonData);
                $po->save();
            }
        }
    }

}
