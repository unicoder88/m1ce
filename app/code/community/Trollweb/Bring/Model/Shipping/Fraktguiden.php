<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 * Visit us at http://www.trollweb.no today!
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2011 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 */

/**
 * Our test shipping method module adapter
 */
class Trollweb_Bring_Model_Shipping_Fraktguiden
    extends Mage_Shipping_Model_Carrier_Abstract
{

    /**
     * unique internal shipping method identifier
     * @var string [a-z0-9_]
     */
    protected $_code = 'bring_fraktguiden';
    protected $_maxweight = 35000;

    protected $_cartWeight = 0;

    protected $_result;
    protected $_missingDstPostcode = false;

    const FRAKTGUIDEN_WSDL = 'fraktguide-v11.wsdl';
    const SPORING_URL = 'http://sporing.posten.no/sporing.json';
    const SIGNATUR_URL = 'http://sporing.posten.no/signatur.png';
    const WEIGHTHANDLING_TOTALWEIGHT = 0;
    const WEIGHTHANDLING_CALCULATE = 1;
    const WEIGHTHANDLING_TOTALWEIGHT_KG = 2;
    const DELIVERY_OFFICE = 'posten';
    const DELIVERY_TERMINAL = 'terminal';

    const ROUND_NONE = 'none';
    const ROUND_DOWN_INT = 'down_int';
    const ROUND_DOWN_TEN = 'down_ten';
    const ROUND_UP_INT = 'up_int';
    const ROUND_UP_TEN = 'up_ten';
    const ROUND_NEAREST_INT = 'nearest_int';
    const ROUND_NEAREST_TEN = 'nearest_ten';

    /**
     * Collect rates for this shipping method based on information in $request
     *
     * @param Mage_Shipping_Model_Rate_Request $data
     *
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->isActiveForRequest($request)) {
            return;
        }

        $overweightLimit = (int)$this->getConfigData('overweight_limit');
        if ($overweightLimit > 0) {
            $this->_maxweight = $overweightLimit*1000;
        }
        $helper = Mage::helper('bring');
        $helper->log("collectRates()");
        // skip if not enabled
        if (!$this->getConfigData('active')) {
            $helper->log('Module is not active');
            return false;
        }
        $free_shipping = false;
        $session = Mage::getSingleton('checkout/session');
        $infoArray = array();

        $bringRequest = Mage::getModel('bring/Shipping_Fraktguiden_Request');
        $this->setBringRequest($bringRequest);

        // Initial setup
        $bringRequest->setUsercode($this->getConfigData('usercode'));
        $bringRequest->setFromPostalCode(
            Mage::getStoreConfig('shipping/origin/postcode', $this->getStore())
        );
        $bringRequest->setFromCountryCode(
            Mage::getStoreConfig('shipping/origin/country_id', $this->getStore())
        );

        $uc = $bringRequest->getUsercode();
        if (empty($uc)) {
            Mage::throwException(Mage::helper('bring')
                                     ->__('No Identifier for Bring Fraktguiden found. Please enter a identifier in the admin system.'));
        }

        $helper->log("DURR");

        // get necessary configuration values
        $handling_type = $this->getConfigData('handling_type');
        $handling = $this->getConfigData('handling_fee');

        // Return if to-postcode is not filled in.
        $topcode = trim($request->getDestPostcode());

        if (empty($topcode)) {
            $this->_missingDstPostcode = true;
        }

        if (empty($topcode) || !preg_match("/^[0-9]{3,5}$/", $topcode)) {
            $topcode = $bringRequest->getFromPostalCode();
        }

        if (!preg_match("/^[0-9]{3,5}$/", $topcode)) {
            $error = Mage::getModel('bring/shipping_carrier_transport_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage(
                $helper->__('%s is not a valid postal code', $topcode)
            );

            return $this->appendResult($error);
        }

        $bringRequest->setToStreet($request->getDestStreet());
        $bringRequest->setToPostalCode($topcode);
        $bringRequest->setToCountryCode($request->getDestCountryId());
        $bringRequest->setLanguage(Mage::getStoreConfig("shipping/origin/country_id"));
        $bringRequest->setEDI(true);
        $bringRequest->setPostingAtPostoffice(($this->getConfigData('posting_site') == self::DELIVERY_OFFICE));

        $bringRequest->setAdditionalServices('EVARSLING');

        // Calculate weight
        $weight = 0;
        $cart_weight = 0;
        $freeBoxes = 0;
        $packagevolume = array();
        $bring_disable = false;

        // Loop trough cart.
        foreach ($request->getAllItems() as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }

            if ($item->getHasChildren() && $item->isShipSeparately()) {
                foreach ($item->getChildren() as $child) {
                    if ($child->getFreeShipping() && !$child->getProduct()
                                                            ->isVirtual()
                    ) {
                        $freeBoxes += $item->getQty() * $child->getQty();
                    }
                }
            } elseif ($item->getFreeShipping()) {
                $freeBoxes += $item->getQty();
            }

            if (($item->getWeight() == 0) and $this->getConfigData('use_average_on_nullweight')) {
                $cart_weight += $item->getQty() * (float)$this->getConfigData('product_weight');
            } else {
                $cart_weight += $item->getQty() * $item->getWeight();
            }

            $product = Mage::getModel('catalog/product')
                           ->setStoreId($item->getProduct()->getStoreId())
                           ->load($item->getProductId());
            foreach ($product->getAttributes() as $attribute => $attrobj) {
                switch ($attribute) {
                    case $this->getConfigData('height_attribute'):
                        $bring_height =
                            $attrobj->getFrontend()->getValue($product);
                        if (empty($bring_height)) {
                            // Use default - if empty
                            $bring_height =
                                $this->getConfigData('default_product_height');
                        } else {
                            // The product has a height, make sure we get the height in cm
                            $bring_height = $helper->getMeasurementInCentimeters($bring_height);
                        }

                        if (!isset($packagevolume['height']) or ($packagevolume['height'] < $bring_height)) {
                            $packagevolume['height'] = $bring_height;
                        }
                        break;
                    case $this->getConfigData('length_attribute'):
                        $bring_length =
                            $attrobj->getFrontend()->getValue($product);
                        if (empty($bring_length)) {
                            // Use default - if empty
                            $bring_length =
                                $this->getConfigData('default_product_length');
                        } else {
                            // The product has a length, make sure we get the length in cm
                            $bring_length = $helper->getMeasurementInCentimeters($bring_length);
                        }

                        if (!isset($packagevolume['length']) or ($packagevolume['length'] < $bring_length)) {
                            $packagevolume['length'] = $bring_length;
                        }
                        break;
                    case $this->getConfigData('width_attribute'):
                        $bring_width =
                            $attrobj->getFrontend()->getValue($product);
                        if (empty($bring_width)) {
                            // Use default - if empty
                            $bring_width =
                                $this->getConfigData('default_product_width');
                        } else {
                            // The product has a width, make sure we get the width in cm
                            $bring_width = $helper->getMeasurementInCentimeters($bring_width);
                        }

                        if (!isset($packagevolume['width']) or ($packagevolume['width'] < $bring_width)) {
                            $packagevolume['width'] = $bring_width;
                        }
                        break;
                    case 'bring_disable':
                        if ($product->getData($attribute) == 1) {
                            $bring_disable = true;
                        }
                }
            }
            unset($product);
        }

        // Do not show the bring method if bring_disabled is set on a product.
        if ($bring_disable) {
            return $this->getResult();
        }

        // Calculate package weight by looping trought all the items and getting the correct weight.
        if (($this->getConfigData('weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT) or
            ($this->getConfigData('weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT_KG)
        ) {
            $weight = $cart_weight;
            // if weight is in KG - multiply with 1000 to get it in Grams.
            if ($this->getConfigData('weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT_KG) {
                $weight = $weight * 1000;
            }
        } else {
            $weight =
                ceil($request->getPackageQty() * (float)$this->getConfigData('product_weight'));
        }

        try {
            $bringClient = Mage::helper("bring")->getBringClient();
        } catch (Exception $e) {
            Mage::log('[Bring module] Unable to load SOAPClient. Make sure the SOAP extension is loaded into PHP.',
                Zend_Log::ERR, '', true);
            $this->dolog('Unable to load SOAPClient. Make sure the SOAP extension is loaded into PHP.');
            $bringClient = false;
        }

        $postOppkravArray = $session->getTrollWebBringPostOppkrav();
        if (!$postOppkravArray) {
            $postOppkravArray = array();
        }
        // Check if free shipping is enabled
        if ($this->getConfigData('free_shipping_enable')) {
            $free_shipping_limit =
                $this->getConfigData('free_shipping_subtotal');
            if ($request->getPackageValue() >= $free_shipping_limit) {
                $free_shipping = true;

                if ($this->getConfigData('free_shipping_hide_other')) {
                    $freeShippingPrice = 0;
                    if ($this->getConfigData('extrafee_enable') && $this->getConfigData('extrafee_onfree') && $this->withinExtraFeeRange($topcode)) {
                        $extraFee = (float)$this->getConfigData('extrafee_amount');
                        $freeShippingPrice += $extraFee;
                    }
                    $method = Mage::getModel('shipping/rate_result_method');
                    $method->setCarrier($this->_code);
                    $method->setCarrierTitle($this->getConfigData('title'));
                    $method->setMethod('FreeShipping');
                    $method->setMethodTitle(Mage::helper('bring')
                                                ->__('Free shipping'));
                    $method->setPrice($freeShippingPrice);
                    $this->appendResult($method);

                    if ($bringClient) {
                        // Fetch COD Price
                        $bringRequest->setPackageGrossWeight(100);
                        $bringRequest->setProductId('MAIL');
                        $bringResult = $bringClient->getPrice($bringRequest);

                        if ($bringResult !== false) {
                            if (property_exists($bringResult->Consignment->Product->Price,
                                'AdditionalServicePrices')) {
                                $price =
                                    $bringResult->Consignment->Product->Price->AdditionalServicePrices;
                                if ((property_exists($price,
                                    'AdditionalService'))
                                ) {
                                    if ($bringResult->Consignment->Product->Price->AdditionalServicePrices->AdditionalService->AdditionalServiceId == "POSTOPPKRAV") {
                                        $postOppkravArray['FreeShipping'] =
                                            $bringResult->Consignment->Product->Price->AdditionalServicePrices->AdditionalService->AdditionalServicePrice;
                                    }
                                }
                            }
                        }
                    }

                    $session->setTrollWebBringPostOppkrav($postOppkravArray);
                    $session->setTrollWebBringFreeShipping(true);

                    // other shipment options is hidden on free shipping return the result now.
                    return $this->getResult;
                }
            }
        }

        //var_dump($request);
        // Check if there is a cart rule for free shipping
        if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeBoxes) {
            $free_shipping = true;
        }

        if (!$bringClient) {
            return $this->getResult();
        }

        $totalWeight = $weight;
        $overweight_kollier = 0;
        if (($weight >= $this->_maxweight) and $this->getConfigData('allow_overweight')) {
            $remaining_weight = $weight % $this->_maxweight;
            $overweight_kollier = floor($weight / $this->_maxweight);
            $weight = $remaining_weight;
            if ($weight == 0) {
                // Make sure the remaining weight isn't 0.
                $weight++;
            }
        }
        $bringRequest->setPackageGrossWeight($weight);

        // Volume
        if ($this->getConfigData('activate_volume')) {
            $bringRequest->setPackageHeight(isset($packagevolume['height']) ?  $packagevolume['height'] : false);
            $bringRequest->setPackageLength(isset($packagevolume['length']) ?  $packagevolume['length'] : false);
            $bringRequest->setPackageWidth(isset($packagevolume['width']) ?  $packagevolume['width'] : false);
        }

        $letterMaxValue = $this->getConfigData('letter_max_value');
        $cartValue = $request->getPackageValue();

        $methods = $this->getActiveMethods();
        $allMethods = $this->getAllMethods();
        $session->setTrollWebBringFreeShipping($free_shipping);
        foreach ($methods as $rMethod) {
            if (!isset($allMethods[$this->_code][$rMethod['shipping_method']])) {
                continue;
            }

            if (
            (($letterMaxValue > 0) and ($cartValue >= $letterMaxValue))
            ) {
                if (($rMethod['shipping_method'] == 'MAIL')) {
                    // Don't show Brevpost if order subtotal is greater than letterMaxValue
                    continue;
                }
            }

            // Check the weight.
            $_minWeight = (float)$rMethod['min_weight'];
            $_maxWeight = (float)$rMethod['max_weight'];

            // Convert min and max weight to be the same unit as the weight setting
            if ($this->getConfigData('weight_handling') == Trollweb_Bring_Model_Shipping_Fraktguiden::WEIGHTHANDLING_TOTALWEIGHT_KG) {
                $_minWeight = $_minWeight * 1000;
                $_maxWeight = $_maxWeight * 1000;
            }

            if ($_minWeight > 0 || $_maxWeight > 0) {
                if ($_minWeight > 0 && $_minWeight > $weight) {
                    // Continue to next method if the weight
                    // is less than cart weight
                    continue;
                }

                if ($_maxWeight > 0 && $_maxWeight < $weight) {
                    // Continue to next method if the weight
                    // is larger than cart weight
                    continue;
                }
            }

            // create new instance of method rate
            $method = Mage::getModel('shipping/rate_result_method');

            // record carrier information
            $method->setCarrier($this->getCarrierCode($rMethod['shipping_method']));
            $method->setCarrierTitle($this->getConfigData('title'));

            // record method information
            $method->setMethod($rMethod['shipping_method']);
            $method->setMethodTitle($this->getMethods($rMethod['shipping_method']));

            if ($rMethod['shipping_method'] == 'POSTAUTOMAT_NEXTDAY') {

                $automat_price = $session->getData('postautomat_price', false);

                if ($automat_price) {
                    $method->setPrice($this->roundPrice($automat_price));
                }

                $this->appendResult($method);
            }

            $destCountrId = strtoupper($request->getDestCountryId());

            // For shipments to Denmark, PICKUP_PARCEL needs to be ordered with the “PICKUP_POINT”
            // additional service to send as a low-cost delivery from Bring’s parcel 
            if ($rMethod['shipping_method'] == "PICKUP_PARCEL" && $destCountrId = "DK") {
                $bringRequest->addAdditionalService('PICKUP_POINT');
            }

            $bringRequest->setProductId($rMethod['shipping_method']);

            $kolli_price = 0;
            if ($overweight_kollier > 0) {
                $tmpweight = $bringRequest->getPackageGrossWeight();
                $bringRequest->setPackageGrossWeight($this->_maxweight);
                $bringResult = $bringClient->getPrice($bringRequest);
                $bringRequest->setPackageGrossWeight($tmpweight); // Restore Grossweight

                if ($bringResult === false) {
                    continue; // Continue if result has errors.
                }

                if (!property_exists($bringResult->Consignment, "Product")) {
                    continue; // Continue if package doesn't exist.
                }

                // Get price and multiply with number of collies.
                if (Mage::getStoreConfig('tax/calculation/shipping_includes_tax')) {
                    $kolli_price =
                        ($bringResult->Consignment->Product->Price->PackagePriceWithoutAdditionalServices->AmountWithVAT * $overweight_kollier);
                } else {
                    $kolli_price =
                        ($bringResult->Consignment->Product->Price->PackagePriceWithoutAdditionalServices->AmountWithoutVAT * $overweight_kollier);
                }
            }

            if ($this->getConfigData('activate_volume') && $this->getConfigData('ensure_minimal_dimensions')) {
                $bringRequest = $this->ensureMinimalDimensions($bringRequest);
            }

            $bringResult = $bringClient->getPrice($bringRequest);

            if ($bringResult === false) {
                // TODO: Add some kind of errorhandling with logging to logfile....
                continue; // Contineu if result has errors.
            }

            if (!property_exists($bringResult->Consignment, "Product")) {
                continue; // Continue if package doesn't exist.
            }

            $bringProduct = $bringResult->Consignment->Product;
            if (property_exists($bringProduct, 'GuiInformation') and property_exists($bringProduct->GuiInformation, 'DisplayName') and $bringProduct->GuiInformation->DisplayName) {
                $method->setMethodTitle($bringProduct->GuiInformation->DisplayName);

                if ($bringProduct->GuiInformation->DescriptionText) {
                    if ($this->getConfigData('show_expected_delivery') && $bringProduct->ExpectedDelivery && $bringProduct->ExpectedDelivery->FormattedExpectedDeliveryDate) {
                        $method->setMethodDescription($bringProduct->GuiInformation->DescriptionText .
                            '<br />' .
                            Mage::helper('bring')->__('Expected delivery: %s', $bringProduct->ExpectedDelivery->FormattedExpectedDeliveryDate));
                    } else {
                        $method->setMethodDescription($bringProduct->GuiInformation->DescriptionText);
                    }
                } else {
                    $method->setMethodDescription('');
                }

                $_key = $this->getCarrierCode($rMethod['shipping_method']).'_'.$rMethod['shipping_method'];
                $infoArray[$_key] = array(
                    'MainDisplayCategory' => $bringProduct->GuiInformation->MainDisplayCategory,
                    'SubDisplayCategory'  => $bringProduct->GuiInformation->SubDisplayCategory,
                    'DisplayName'         => $bringProduct->GuiInformation->DisplayName,
                    'ProductName'         => $bringProduct->GuiInformation->ProductName,
                    'DescriptionText'     => $bringProduct->GuiInformation->DescriptionText,
                    'HelpText'            => $bringProduct->GuiInformation->HelpText,
                    //'ExpectedDelivery'    => $bringProduct->
                );
            } else {
                // Temporary fix until bring add DisplayName to this product in the api
                $bringProductId = $bringProduct->ProductId;
                if ($bringProductId == "MAIL") {
                    $method->setMethodTitle("Brevpost");
                }
            }

            $additionalServicePrice = 0;
            if (property_exists($bringProduct,
                    'Price') && property_exists($bringProduct->Price,
                    'AdditionalServicePrices')
            ) {
                $price = $bringProduct->Price->AdditionalServicePrices;
                if ((property_exists($price, 'AdditionalService'))) {
                    if (is_array($price->AdditionalService)) {
                        foreach ($price->AdditionalService as $service) {
                            if ($service->AdditionalServiceId == "POSTOPPKRAV") {
                                $postOppkravArray[$rMethod['shipping_method']] =
                                    $service->AdditionalServicePrice;
                            } elseif ($service->AdditionalServiceId == "PICKUP_POINT" && $rMethod['shipping_method'] == "PICKUP_PARCEL" && $destCountrId == "DK") {
                                if (Mage::getStoreConfig('tax/calculation/shipping_includes_tax')) {
                                    $additionalServicePrice = $service->AdditionalServicePrice->AmountWithVAT;
                                } else {
                                    $additionalServicePrice = $service->AdditionalServicePrice->AmountWithoutVAT;
                                }
                            }
                        }
                    } else {
                        if ($price->AdditionalService->AdditionalServiceId == "POSTOPPKRAV") {
                            $postOppkravArray[$rMethod['shipping_method']] =
                                $price->AdditionalService->AdditionalServicePrice;
                        }
                    }
                }
            }

            if (property_exists($bringProduct,
                    'Price') && Mage::getStoreConfig('tax/calculation/shipping_includes_tax')
            ) {
                $price =
                    $bringProduct->Price->PackagePriceWithoutAdditionalServices->AmountWithVAT;
            } elseif (property_exists($bringProduct, 'Price')) {
                $price =
                    $bringProduct->Price->PackagePriceWithoutAdditionalServices->AmountWithoutVAT;
            } elseif ((float)$rMethod['custom_price'] > 0) {
                $price = (float)$rMethod['custom_price'];
            } else {
                continue;
            }

            if ($handling_type == Mage_Shipping_Model_Carrier_Abstract::HANDLING_TYPE_PERCENT) {
                $handling_fee = $price * ($handling / 100);
            } else {
                $handling_fee = $handling;
            }

            $extraFee = 0;
            if ($this->getConfigData('extrafee_enable') && $this->withinExtraFeeRange($topcode)) {
                $extraFee = (float)$this->getConfigData('extrafee_amount');
            }

            // rate cost is optional property to record how much it costs to vendor to ship
            $method->setCost($price + $kolli_price);
            if (isset($bringProduct->ExpectedDelivery->FormattedExpectedDeliveryDate)) {
                $method->addData(array('expected_delivery' => $bringProduct->ExpectedDelivery->FormattedExpectedDeliveryDate));
            }

            if ($free_shipping and $rMethod['allow_free_shipping']) {
                $freeShippingPrice = 0;
                if ($this->getConfigData('extrafee_onfree')) {
                    $freeShippingPrice += $extraFee;
                }
                $method->setPrice($freeShippingPrice);
                $this->appendResult($method);
            } else {
                if ($request->getDestCountryId() === "NO" && isset($rMethod['price_to_no']) && (float)$rMethod['price_to_no'] > 0) {
                    $method->setPrice($rMethod['price_to_no']);
                } elseif ($request->getDestCountryId() === "SE" && (float)$rMethod['price_to_se'] > 0) {
                    $method->setPrice($rMethod['price_to_se']);
                } elseif ($request->getDestCountryId() === "DK" && (float)$rMethod['price_to_dk'] > 0) {
                    $method->setPrice($rMethod['price_to_dk']);
                } elseif ($request->getDestCountryId() === "FI" && (float)$rMethod['price_to_fi'] > 0) {
                    $method->setPrice($rMethod['price_to_fi']);
                } elseif ((float)$rMethod['custom_price'] > 0) {
                    if ($overweight_kollier > 0) {
                        $customPrice = $rMethod['custom_price']*($overweight_kollier+1);
                    }
                    else {
                        $customPrice = $rMethod['custom_price'];
                    }
                    $method->setPrice($customPrice + $extraFee);
                } else {
                    $method->setPrice($extraFee + $handling_fee + $price + $kolli_price + $additionalServicePrice);
                }

                // If the price is 0, just skip the type.
                if ($price > 0) {
                    if (((float)$this->getConfigData('shipping_discount')) > 0) {
                        $method->setPrice($method->getPrice() * ((100 - $this->getConfigData('shipping_discount')) / 100));
                    }

                    // Round the price
                    $method->setPrice($this->roundPrice($method->getPrice()));

                    // add this rate to the result
                    $this->appendResult($method);
                }
            }
        }
        $session->setTrollWebBringPostOppkrav($postOppkravArray);
        if ($this->getConfigData('tooltip_active')) {
            $existingInfoArray = $session->getTrollwebBringInfo();
            if (!$existingInfoArray) {
                $existingInfoArray = array();
            }
            $newInfoArray = array_merge($existingInfoArray, $infoArray);
            $session->setTrollwebBringInfo($newInfoArray);
        }

        return $this->getResult();
    }

    /**
     * Get tracking info about the shipment.
     *
     * @param $tracking
     *
     * @return Mage_Shipping_Model_Tracking_Result_Status
     */
    public function getTrackingInfo($tracking)
    {
        return $this->getTracking($tracking);
        /** Depricated
         * $status = Mage::getModel('shipping/tracking_result_status');
         * $status->setCarrier('bring');
         * $status->setCarrierTitle($this->getConfigData('title'));
         * $status->setTracking($tracking);
         * $status->setPopup(1);
         * $status->setUrl("http://sporing.posten.no/sporing.html?q=".$tracking);
         * $status->setTrackSummary('<a href="'.$status->getUrl().'" onclick="this.target=\'_blank\'">Klikk her for å spore forsendelsen</a>');
         * return $status;
         */
    }

    protected function appendResult($method)
    {
        if (!isset($this->_result)) {
            $this->_result = Mage::getModel('shipping/rate_result');
        }

        if ($this->_missingDstPostcode && $method->getMethod() == "SERVICEPAKKE") {
            $method->setMethodDescription(''); // Remove shipping description for "Servicepakke"
        }

        $this->_result->append($method);

        return $this->_result;
    }

    protected function getResult()
    {
        return $this->_result;
    }

    private function getTracking($tracking)
    {
        $url =
            Trollweb_Bring_Model_Shipping_Fraktguiden::SPORING_URL.'?q='.$tracking;

        try {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "User-Agent: curl/".$this->getStoreName(),
            ));
            $responseBody = curl_exec($ch);
            curl_close($ch);
        } catch (Exception $e) {
            $responseBody = "";
        }

        $json = json_decode($responseBody);

        $info = $json->consignmentSet[0];

        $resultArr = array();
        $packageProgress = array();
        if (isset($info->packageSet[0])) {
            $resultArr['service'] = $info->packageSet[0]->productName;
            $resultArr['weight'] = $info->totalWeightInKgs." Kg";

            foreach ($info->packageSet[0]->eventSet as $event) {
                if (!isset($resultArr['status'])) {
                    $resultArr['status'] = $event->description;
                }

                if (!isset($resultArr['deliverydate'])) {
                    $resultArr['deliverydate'] = $event->displayDate;
                    $resultArr['deliverytime'] = $event->displayTime.":00";
                }

                if (isset($event->recipientSignature->name) and !isset($resultArr['signedby'])) {
                    $signdate = explode('+', $event->dateIso);
                    $sign_url =
                        Trollweb_Bring_Model_Shipping_Fraktguiden::SIGNATUR_URL.'?kollinummer='.$info->packageSet[0]->packageNumber.'&amp;date='.$signdate[0];
                    $resultArr['signedby'] =
                        '<a onClick="popWin(\''.$sign_url.'\',\'signature\',\'width=400,height=200,resizable=yes,scrollbars=no\')" href="#">'.$event->recipientSignature->name.'</a>';
                    //$resultArr['signedby'] = $event->recipientSignature->name;
                }

                $tmpArr = array();
                $tmpArr['activity'] = $event->description;
                $tmpArr['deliverydate'] = (string)$event->displayDate;
                $tmpArr['deliverytime'] = $event->displayTime.':00';
                $tmpArr['deliverylocation'] =
                    $event->postalCode." ".$event->city;

                $packageProgress[] = $tmpArr;
            }
            $resultArr['progressdetail'] = $packageProgress;
        }

        $status = Mage::getModel('shipping/tracking_result_status');
        $status->setCarrier('bring');
        $status->setCarrierTitle($this->getConfigData('title'));
        $status->setTracking($tracking);
        if (isset($info->error->message)) {
            $status->setTrackSummary($info->error->message);
        } else {
            $status->addData($resultArr);
        }

        return $status;
    }

    /**
     * Get allowed shipping methods
     * @return array
     */
    public function getAllowedMethods()
    {
        $available = array();
        $methods = $this->getActiveMethods();
        $allMethods = $this->getAllMethods();

        foreach ($methods as $method) {
            if (!isset($allMethods[$this->_code][$method['shipping_method']])) {
                continue;
            }
            $available[$method['shipping_method']] =
                $this->getMethods($method['shipping_method']);
        }

        return $available;
    }

    /*
    public function isZipCodeRequired()
    {
        return false;
    }
    */

    public function isTrackingAvailable()
    {
        return true;
    }

    public function roundPrice($price)
    {
        switch ($this->getConfigData('round_prices')) {
            case self::ROUND_NEAREST_INT:
                return round($price, 0);
            case self::ROUND_NEAREST_TEN:
                return round($price, -1);

            case self::ROUND_UP_INT:
                return ceil($price);
            case self::ROUND_UP_TEN:
                if ($price == 0) {
                    return 0;
                }
                if ($price < 10) {
                    return 10;
                }

                return ceil($price / 10) * 10;

            case self::ROUND_DOWN_INT:
                return floor($price);
            case self::ROUND_DOWN_TEN:
                if ($price < 10) {
                    return 0;
                }

                return floor($price / 10) * 10;

            case self::ROUND_NONE:
            default:
                return $price;
        }
    }

    public function getMethods($name = null)
    {
        $codes = array();
        foreach ($this->getAllMethods() as $type => $methods) {

            foreach ($methods as $code => $mname) {
                $codes[$code] = $mname;
            }
        }

        if ($name !== null) {
            if (isset($codes[$name])) {
                return $codes[$name];
            } else {
                return false;
            }
        } else {
            return $codes;
        }
    }

    public function getCarrierCode($method = null)
    {

        $codes = $this->getAllMethods();

        if ($method) {
            foreach ($codes as $code => $methods) {
                if (in_array($method, array_keys($methods))) {
                    return $code;
                }
            }
        }

        return $this->_code;
    }

    public function getAllMethods()
    {
        $helper = Mage::helper('bring');
        $codes = array(
            'bringdoren'  => array(),
            'bringlevert' => array(
                'PA_DOREN'                            => $helper->__('På døren'),
                'BPAKKE_DOR-DOR'                      => $helper->__('Bedriftspakke Dør-Dør'),
                'EKSPRESS09'                          => $helper->__('Ekspress 09'),
                'MAIL'                                => $helper->__('Brevpost'),
                'PAKKE_I_POSTKASSEN'                  => $helper->__('Pakke i postkassen'),
                'PAKKE_I_POSTKASSEN_SPORBAR'          => $helper->__('Pakke i postkassen sporbar'),
                'EXPRESS_NORDIC_SAME_DAY'             => $helper->__('Express Nordic Same Day'),
                'EXPRESS_INTERNATIONAL_0900'          => $helper->__('Express International 09:00'),
                'EXPRESS_INTERNATIONAL_1200'          => $helper->__('Express International 12:00'),
                'EXPRESS_INTERNATIONAL'               => $helper->__('Express International'),
                'EXPRESS_NORDIC_0900'                 => $helper->__('Express Nordic 0900'),
                'EXPRESS_ECONOMY'                     => $helper->__('Express Economy'),
                'CARGO_GROUPAGE'                      => $helper->__('Cargo'),
                'CARGO_INTERNATIONAL'                 => $helper->__('Cargo International'),
                'BUSINESS_PARCEL'                     => $helper->__('Business Parcel'),
                'COURIER_VIP'                         => $helper->__('Bud VIP'),
                'COURIER_1H'                          => $helper->__('Bud 1 time'),
                'COURIER_2H'                          => $helper->__('Bud 2 timer'),
                'COURIER_4H'                          => $helper->__('Bud 4 timer'),
                'COURIER_6H'                          => $helper->__('Bud 6 timer'),
                'HOME_DELIVERY_PARCEL'                => $helper->__('Home delivery'),
                'FRIGO'                               => $helper->__('Frigo'),
            ),
            'bringhent'   => array(
                'SERVICEPAKKE'                         => $helper->__('Servicepakke'),
                'PICKUP_PARCEL'                        => Mage::helper('bring')->__('Pickup Parcel'),
                // Fake shipping method
            ),
        );

        return $codes;
    }

    public function getActiveMethods()
    {
        if (($methods =
                @unserialize($this->getConfigData('allowed_methods'))) == false
        ) {
            // New config failed - try to load the oldfashion way.
            $methods = array();
            $old_methods =
                explode(",", $this->getConfigData('allowed_methods'));
            foreach ($old_methods as $method) {
                $methods[] =
                    array(
                        'shipping_method'     => $method,
                        'custom_price'        => 0,
                        'allow_free_shipping' => 1,
                    );
            }
        }

        return $methods;
    }

    /**
     * Retrieve information from carrier configuration
     *
     * @param   string $field
     *
     * @return  mixed
     */
    public function getConfigData($field)
    {
        if (in_array($field, array('active', 'sort_order', 'title'))) {
            return parent::getConfigData($field);
        }
        // To be backwards compatible with old settings.
        $path = 'carriers/bring_fraktguiden/'.$field;

        return Mage::getStoreConfig($path, $this->getStore());
    }

    private function dolog($logline)
    {
        $logDir = Mage::getBaseDir('log');
        $fh = fopen($logDir."/trollweb_bring.log", "a");
        if ($fh) {
            fwrite($fh, "[".date("d.m.Y h:i:s")."] ".$logline."\n");
            fclose($fh);
        }
    }

    public function saveConfigData()
    {
        $helper = Mage::helper('bring');
        $client = new Zend_Http_Client();
        $client->setUri('http://serial.trollweb.no/bring.php');

        $client->setConfig(array(
            'adapter'     => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => array(
                CURLOPT_USERAGENT      => 'Serial Reporter',
                CURLOPT_HEADER         => 0,
                CURLOPT_VERBOSE        => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT        => 10,
            ),
        ));

        $client->setMethod(Zend_Http_Client::POST);

        $url = Mage::app()->getWebsite()->getConfig('web/unsecure/base_url');
        $domain = trim(preg_replace('/^.*?\\/\\/(.*)?\\//', '$1', $url));

        $storeName = Mage::app()->getStore()->getFrontendName();

        $client->setParameterPost('domain', $domain);
        $client->setParameterPost('magento_version', Mage::getVersion());
        $client->setParameterPost(
            'bring_version',
            $helper->getVersion()
        );

        $errorMessage =
            'Unable to save Bring configuration, please try again or contact support@trollweb.no';

        try {
            $response = $client->request();

            if ($response->getBody() != 'OK') {
                Mage::getSingleton('adminhtml/session')
                    ->addError($helper->__($errorMessage));
            }
        } catch (Exception $e) {
            $this->dolog($e->getMessage());
            Mage::getSingleton('adminhtml/session')
                ->addError($helper->__($errorMessage));
        }

        return $this;
    }

    private function isActiveForRequest(Mage_Shipping_Model_Rate_Request $request) {
        $event = new Varien_Object();
        $event->setData(array(
            "request" => $request,
            "is_active" => true,
        ));
        Mage::dispatchEvent('trollweb_bring_collect_rates_before', array('event' => $event));

        return $event->getData("is_active");
    }



    // Packages with smaller dimensions than 23x13x1 gets a spesialgodstillegg
    // make sure that the package does not have smaller dimensions than this.
    private function ensureMinimalDimensions($bringRequest) {
        $ignoredProducts = ["MAIL"];
        $productId = $bringRequest->getProductId();

        if (in_array($productId, $ignoredProducts)) {
            return $bringRequest;
        }

        $dimensions = [
            $bringRequest->getPackageHeight(),
            $bringRequest->getPackageWidth(),
            $bringRequest->getPackageLength(),
        ];

        // Make the highest value element first in the array
        sort($dimensions, SORT_NUMERIC);
        $dimensions = array_reverse($dimensions);

        if ($dimensions[0] < 23) {
            $dimensions[0] = 23;
        }

        if ($dimensions[1] < 13) {
            $dimensions[1] = 13;
        }

        if ($dimensions[2] < 1) {
            $dimensions[2] = 1;
        }

        $bringRequest->setPackageHeight($dimensions[0]);
        $bringRequest->setPackageWidth($dimensions[1]);
        $bringRequest->setPackageLength($dimensions[2]);

        return $bringRequest;
    }

    private function withinExtraFeeRange($destination)
    {
        $postcodes = $this->getConfigData('extrafee_postcodes');
        $pArray = explode(",",$postcodes);

        $withinRange = false;
        foreach ($pArray as $pCode) {
          if (stristr($pCode,'-')) {
            list($fpCode,$tpCode) = explode("-",$pCode);
            if ($destination >= $fpCode and $destination <= $tpCode) {
              $withinRange = true;
              break;
            }
          }
          else {
            if ($destination == $pCode) {
              $withinRange = true;
              break;
            }
          }
        }

        return $withinRange;
    }
}
